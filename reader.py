# encoding=utf-8
from __future__ import unicode_literals, print_function

import sys
import datetime
import time

from main import capture_exception
from source.interface import Interface
from source.exceptions import ConnectionProblem
from config import config

if __name__ == "__main__":
    reader = None  # type: Interface

    if "--wait" in sys.argv:
        time.sleep(120)

    try:
        for num in range(10):
            port = "/dev/ttyUSB"+str(num)
            try:
                reader = Interface(port)
            except ConnectionProblem:
                pass
            else:
                print("Connected to port %s at %s" % (port, datetime.datetime.now()))
                break

        if reader is None:
            print("No reader found")
        else:
            print("Running automatic reading at %s" % (datetime.datetime.now()))
            reader.automatic_reading(config["filename"], 1)
    except:
        capture_exception(reader, config.get("sentry_dsn", None))
