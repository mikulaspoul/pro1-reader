This is a small util for reading data from Pro1-Mod electricity meter.
It can run on it's own via the file `reader.py` or can be controlled automatically via `main.py`

When run automatically, it can be setup to run on a Raspberry Pi using supervisor (`pro1_reader.conf`)
