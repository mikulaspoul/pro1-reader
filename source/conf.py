# encoding=utf-8
from __future__ import unicode_literals, print_function

import os
from unipath import Path


DECIMALS = 15

if os.name == "nt":
    DEFAULT_PORT = "COM3"
else:
    DEFAULT_PORT = "/dev/ttyUSB0"

DEFAULT_FILE_LOCATION = Path().absolute()
