# encoding=utf-8
from __future__ import unicode_literals, print_function

import serial
import logging

from .exceptions import ConnectionProblem
from .instruments import Pro1Instrument
from .utils import AutoNumber


class Pro1Reader(object):

    # all the values in the registers
    class Attributes(AutoNumber):
        serial_number = ()
        meter_code = ()
        meter_id = ()
        baudrate = ()
        protocol_version = ()
        software_version = ()
        hardware_version = ()
        meter_amps = ()
        s0_output_rate = ()
        combined_code = ()
        lcd_cycle_time = ()
        voltage = ()
        grid_frequency = ()
        current = ()
        total_active_power = ()
        total_reactive_power = ()
        total_apparent_power = ()
        power_factor = ()
        tariff = ()

        total_active_energy = ()
        t1_total_active_energy = ()
        t2_total_active_energy = ()

        forward_active_energy = ()
        t1_forward_active_energy = ()
        t2_forward_active_energy = ()

        reverse_active_energy = ()
        t1_reverse_active_energy = ()
        t2_reverse_active_energy = ()

        total_reactive_energy = ()
        t1_total_reactive_energy = ()
        t2_total_reactive_energy = ()

        forward_reactive_energy = ()
        t1_forward_reactive_energy = ()
        t2_forward_reactive_energy = ()

        reverse_reactive_energy = ()
        t1_reverse_reactive_energy = ()
        t2_reverse_reactive_energy = ()

    def __init__(self, device="/dev/ttyUSB0", debug=False, mock=False):
        """
        :type device: str or unicode
        :param device: Port to which the device is connected
        :param bool debug: To pass to the instrument
        :param bool mock: Return bogus values if the device is not connected
        """
        self.mock = mock
        self.logger = logging.getLogger("pro1_reader")

        if not mock:
            try:
                self.instrument = Pro1Instrument(device, 1)
                self.instrument.debug = debug
                self.instrument.serial.baudrate = 9600
                self.instrument.serial.timeout = 5
                self.instrument.serial.parity = serial.PARITY_EVEN
            except serial.serialutil.SerialException:
                self.instrument = None
                raise ConnectionProblem

        # Definition of attributes
        # First the function used to read the attribute, then the register address and for int the number of registered
        self.definition = {
            self.Attributes.serial_number:              ["read_int", 0x1000, 4],
            self.Attributes.meter_code:                 ["read_int", 0x1010, 2],
            self.Attributes.meter_id:                   ["read_int", 0x1018, 1],
            self.Attributes.baudrate:                   ["read_int", 0x1020, 1],
            self.Attributes.protocol_version:           ["read_int", 0x1050, 2],
            self.Attributes.software_version:           ["read_int", 0x1054, 2],
            self.Attributes.hardware_version:           ["read_int", 0x1058, 2],
            self.Attributes.meter_amps:                 ["read_int", 0x1060, 1],

            self.Attributes.s0_output_rate:             ["read_float", 0x1066],

            self.Attributes.combined_code:              ["read_int", 0x107A, 1],
            self.Attributes.lcd_cycle_time:             ["read_int", 0x1510, 1],

            self.Attributes.voltage:                    ["read_float", 0x2000],
            self.Attributes.grid_frequency:             ["read_float", 0x2020],
            self.Attributes.current:                    ["read_float", 0x2060],
            self.Attributes.total_active_power:         ["read_float", 0x2080],
            self.Attributes.total_reactive_power:       ["read_float", 0x20A0],
            self.Attributes.total_apparent_power:       ["read_float", 0x20C0],
            self.Attributes.power_factor:               ["read_float", 0x20E0],

            self.Attributes.tariff:                     ["read_int", 0x2200, 1],

            self.Attributes.total_active_energy:        ["read_float", 0x3000],
            self.Attributes.t1_total_active_energy:     ["read_float", 0x3100],
            self.Attributes.t2_total_active_energy:     ["read_float", 0x3200],

            self.Attributes.forward_active_energy:      ["read_float", 0x3020],
            self.Attributes.t1_forward_active_energy:   ["read_float", 0x3120],
            self.Attributes.t2_forward_active_energy:   ["read_float", 0x3220],

            self.Attributes.reverse_active_energy:      ["read_float", 0x3040],
            self.Attributes.t1_reverse_active_energy:   ["read_float", 0x3140],
            self.Attributes.t2_reverse_active_energy:   ["read_float", 0x3240],

            self.Attributes.total_reactive_energy:      ["read_float", 0x3060],
            self.Attributes.t1_total_reactive_energy:   ["read_float", 0x3160],
            self.Attributes.t2_total_reactive_energy:   ["read_float", 0x3260],

            self.Attributes.forward_reactive_energy:    ["read_float", 0x3080],
            self.Attributes.t1_forward_reactive_energy: ["read_float", 0x3180],
            self.Attributes.t2_forward_reactive_energy: ["read_float", 0x3280],

            self.Attributes.reverse_reactive_energy:    ["read_float", 0x30A0],
            self.Attributes.t1_reverse_reactive_energy: ["read_float", 0x31A0],
            self.Attributes.t2_reverse_reactive_energy: ["read_float", 0x32A0],
        }

    def read_int(self, register, blocks):
        if blocks == 4:
            self.logger.debug("Reading %s, blocks: %s", register, blocks)
            if self.mock:
                return 6465498765465

            return self.instrument.read_longlong(register)

        elif blocks == 2:
            self.logger.debug("Reading %s, blocks: %s", register, blocks)
            if self.mock:
                return 654654

            return self.instrument.read_long(register, signed=True)
        else:
            self.logger.debug("Reading %s, blocks: %s", register, blocks)
            if self.mock:
                return 1

            return self.instrument.read_register(register, signed=True)

    def read_float(self, register):
        self.logger.debug("Reading float from: %s", register)
        if self.mock:
            return register + 0.0000332

        return self.instrument.read_float(register)

    def read(self, value):
        tries = 0
        while True:
            if tries >= 20:
                self.logger.error("Reading %s failed", value)
                raise ValueError("Reading {} failed".format(value))
            try:
                get_definition = self.definition[value]

                return getattr(self, get_definition[0])(*get_definition[1:])
            except (KeyError, AttributeError):
                raise KeyError
            except ValueError:
                tries += 1
