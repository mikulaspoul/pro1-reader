# encoding=utf-8
from __future__ import unicode_literals, print_function

import minimalmodbus


class Pro1Instrument(minimalmodbus.Instrument):

    def read_longlong(self, register_address, function_code=3):
        x = self.read_registers(register_address, 4, functioncode=function_code)

        [a, b, c, d] = x

        return (d << 48) + (c << 32) + (b << 16) + a
