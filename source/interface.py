# encoding=utf-8

import csv
import datetime
import os
import time

import re
from typing import List

from slugify import slugify
from unipath.path import Path

from source.conf import DECIMALS
from source.exceptions import EndProgram
from source.reader import Pro1Reader
from source.utils import get_last_config, update_last_config


class Interface(object):

    # INITIAL_TABLE_PRINT = "__all__"  # print all possible attributes
    INITIAL_TABLE_PRINT = [
        # Pro1Reader.Attributes.serial_number,
        Pro1Reader.Attributes.total_active_energy
    ]  # just some

    SERIALIZER_CONFIG = {
        # "Voltage": Pro1Reader.Attributes.voltage,
        # "Current": Pro1Reader.Attributes.current,

        "Total active power": Pro1Reader.Attributes.total_active_power,
        # "Total reactive power": Pro1Reader.Attributes.total_reactive_power,

        "Total active energy": Pro1Reader.Attributes.total_active_energy,
        # "Forward active energy": Pro1Reader.Attributes.forward_active_energy,
        # "Reverse active energy": Pro1Reader.Attributes.reverse_active_energy,

        # "Total reactive energy": Pro1Reader.Attributes.total_reactive_energy,
        # "Forward reactive energy": Pro1Reader.Attributes.forward_active_energy,
        # "Reverse reactive energy": Pro1Reader.Attributes.reverse_reactive_energy
    }

    def __init__(self, port, mock=False):
        self.reader = Pro1Reader(port, mock=mock)
        self.port = port
        self.location = None
        self.measurement_name = None
        self.periodicity = None
        self.filename = None

    def get_config(self):
        return {
            "port": self.port,
            "location": self.location,
            "measurement_name": self.measurement_name,
            "periodicity": self.periodicity,
            "filename": self.filename,
            "initial_table_print": self.INITIAL_TABLE_PRINT,
            "serializer_config": self.SERIALIZER_CONFIG
        }

    @staticmethod
    def get_length(val):
        """ Returns the length of the stringyfied object, with overwritten case for float for unscientific format
        """
        if isinstance(val, float):
            return len("{:.{decimals}f}".format(val, decimals=DECIMALS))
        return len(val.__str__())

    def main(self):
        """ The main function, prints the table first and then it starts the reading to csv
        """
        if self.INITIAL_TABLE_PRINT == "__all__":
            self.print_table(list(Pro1Reader.Attributes))
        else:
            self.print_table(self.INITIAL_TABLE_PRINT)

        self.save_to_csv()

    def print_table(self, attributes):
        # type: (List) -> None
        """ Prints a nice table of attributes sent via `attributes`
        """
        values = []
        for attr in attributes:
            values.append((attr, self.reader.read(attr)))

        max_length_description = max([len(val[0].__str__()) for val in values])
        max_length_values = max([self.get_length(val[1]) for val in values])

        for val in values:
            print("+-{}-+-{}-+".format("-" * max_length_description, "-" * max_length_values))

            if isinstance(val[1], float):
                format_string = "| {:<{desc_width}} | {:>{val_width}.{decimals}f} |"
            else:
                format_string = "| {:<{desc_width}} | {:>{val_width}} |"

            print(format_string.format(val[0], val[1],
                                       desc_width=max_length_description,
                                       val_width=max_length_values,
                                       decimals=DECIMALS))

        print("+-{}-+-{}-+".format("-" * max_length_description, "-" * max_length_values))

    def get_save_location(self):
        """ Returns an existing path to directory where the files should be saved.
        """
        last_file_location = get_last_config("file_location")

        location = input("Enter where to save the file: (\"{}\" if empty): ".format(
            last_file_location
        ))

        if not location:
            location = last_file_location

        location = Path(location).absolute()

        if not location.exists() or not location.isdir():
            print("The location doesn't exists or is not a directory")
            return self.get_save_location()

        # update last_config for easier use for next launch
        if location != Path(last_file_location):
            update_last_config("file_location", location)

        return location

    def get_measurement_name(self):
        """ Returns a nonempty string of the name of the measurement
        """
        name = input("Enter the name of the measurement: ")

        if not name:
            print("You have to input the name.")
            return self.get_measurement_name()

        return name.strip()

    @staticmethod
    def get_filename(location, measurement_name):
        # type: (Path, str) -> Path
        """ Gets the filename for the data cvs considering the location name of the measurement
              - slugifies the name
              - checks if any files of the same measurement exist in the location -> sets the next one in series
        """
        slugified = slugify(measurement_name)
        current_contents = location.listdir(names_only=True, pattern="*.csv".format(slugified))

        format_string = "{}-[0-9]{{4}}.csv".format(slugified)
        filename_format = re.compile(format_string)

        matching_format = [x for x in current_contents if filename_format.match(x)]
        if not len(matching_format):
            order_of_measurement = 1
        else:
            matching_format.sort(reverse=True)
            last = matching_format[0]
            last = int(last.replace("{}-".format(slugified), "").replace(".csv", ""))

            order_of_measurement = last + 1

        return Path(location, "{}-{:04d}.csv".format(slugified, order_of_measurement))

    def get_periodicity(self):
        """ Gets valid periodicity for the measurement

        :rtype: int or None
        """
        periodicity = input("Get periodicity of measurement in minutes (manual if empty): ")

        if not periodicity.strip():
            return None

        try:
            periodicity = int(periodicity)
        except ValueError:
            print("Not a valid number")
            return self.get_periodicity()

        if periodicity > 60 or periodicity <= 0:
            print("Period must be from 1 to 60")
            return self.get_periodicity()

        return periodicity

    @staticmethod
    def format_to_write(val):
        """ Serializes values to csv
        """
        if isinstance(val, float):
            return "{:.{decimals}f}".format(val, decimals=DECIMALS)
        elif isinstance(val, datetime.date):
            return val.isoformat()
        elif isinstance(val, datetime.time):
            return val.strftime("%H:%M:%S.%f")
        return val

    @staticmethod
    def get_writer(fl):
        return csv.writer(fl, delimiter=";", lineterminator="\n")

    def read_row(self):
        for key in sorted(self.SERIALIZER_CONFIG.keys()):
            try:
                x = self.SERIALIZER_CONFIG[key]
                yield self.format_to_write(self.reader.read(x)) or ""
            except ValueError:
                yield ""

    def read_row_to_csv(self, filename):
        """ Reads a row and saves it to the file
        """
        row = [
                  self.format_to_write(datetime.datetime.now().date()),
                  self.format_to_write(datetime.datetime.now().time())
        ] + list(self.read_row())

        if not filename.exists():
            print("File was deleted during reading.")
            raise EndProgram

        with open(filename, "a+") as fl:
            writer = self.get_writer(fl)
            writer.writerow(row)

    def save_to_csv(self):
        """ Asks for directions and then saves data do a csv
        """
        try:
            self.location = location = self.get_save_location()
        except (EOFError, KeyboardInterrupt):
            print("")
            raise EndProgram

        try:
            self.measurement_name = measurement_name = self.get_measurement_name()
        except (EOFError, KeyboardInterrupt):
            print("")
            raise EndProgram

        self.filename = filename = self.get_filename(location, measurement_name)

        try:
            self.periodicity = periodicity = self.get_periodicity()
        except (EOFError, KeyboardInterrupt):
            print("")
            raise EndProgram

        self.write_header(filename, measurement_name)

        if periodicity is None:  # manual control
            while True:
                try:
                    val = input("Press enter to record data, write anything and press enter to quit: ")

                    if not val:
                        self.read_row_to_csv(filename)
                    else:
                        break
                except (KeyboardInterrupt, EOFError):
                    print("")
                    break

        else:  # periodic control
            self.periodic_reading()

    def write_header(self, filename, measurement_name):
        with open(filename, "w+") as fl:
            writer = self.get_writer(fl)
            writer.writerow([
                measurement_name,
                # self.reader.read(Pro1Reader.Attributes.serial_number)
                "PRO1-Mod"
            ])
            writer.writerow([])
            writer.writerow(["Date", "Time"] + list(sorted(self.SERIALIZER_CONFIG.keys())))

    def automatic_reading(self, filename, periodicity):
        self.filename = Path(filename)
        self.periodicity = periodicity

        if not os.path.exists(filename):
            self.write_header(filename, "Auto reader")

        print("Starting periodic reading at %s" % datetime.datetime.now())
        self.periodic_reading()

    def periodic_reading(self):
        now = datetime.datetime.now()

        # set the minute to run the reader for the first time
        next_run = now.minute + self.periodicity - now.minute % self.periodicity
        if next_run == 60:
            next_run = 0

        print("Next read is at %s" % next_run)

        while True:
            try:
                now = datetime.datetime.now()

                if now.minute % self.periodicity == 0 and (next_run is None or now.minute == next_run):
                    print("Reading data - {}".format(now))
                    self.read_row_to_csv(self.filename)

                    # set at what minute will the reader be run next
                    next_run = now.minute + self.periodicity
                    if next_run == 60:
                        next_run = 0

                time.sleep(1)
            except KeyboardInterrupt:
                print("")
                break
