# encoding=utf-8
from __future__ import unicode_literals, print_function


class ConnectionProblem(Exception):
    pass


class EndProgram(Exception):
    pass
