# encoding=utf-8
from __future__ import unicode_literals, print_function
import json
from enum import Enum
from unipath import Path

from source.conf import DEFAULT_PORT, DEFAULT_FILE_LOCATION


def get_last_config(name):
    """ Gets the last config or the default value
    """
    if name not in {"port", "file_location", "sentry_dsn"}:
        raise ValueError("Invalid name")

    try:
        with open(Path(Path(__file__).parent.parent, "last_config.json")) as fl:
            data = json.loads(fl.read())

        if name != "sentry_dsn":
            res = data[name]
        else:
            res = data.get("sentry_dsn", None)

    except (IOError, ValueError, KeyError):
        if name != "sentry_dsn":
            res = DEFAULT_PORT if name == "port" else DEFAULT_FILE_LOCATION
        else:
            res = None

    return res


def update_last_config(name, value):
    """ Saves the used value
    """
    if name not in {"port", "file_location"}:
        raise ValueError("Invalid name")

    with open(Path(Path(__file__).parent.parent, "last_config.json"), "r+") as fl:
        try:
            data = json.loads(fl.read())
        except (IOError, ValueError):
            data = {
                "port": DEFAULT_PORT,
                "file_location": DEFAULT_FILE_LOCATION
            }

        fl.seek(0)
        fl.truncate()

        data[name] = value

        fl.write(json.dumps(data))


class AutoNumber(Enum):
    def __new__(cls):
        value = len(cls.__members__) + 1
        obj = object.__new__(cls)
        obj._value_ = value
        return obj
