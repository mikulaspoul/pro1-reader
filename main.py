# encoding=utf-8
from __future__ import unicode_literals, print_function

import datetime
import platform
import pprint
import traceback
from urllib.error import URLError, HTTPError

import sys
from raven import Client
from raven.transport.http import HTTPTransport
from unipath import Path

from source.interface import Interface
from source.exceptions import ConnectionProblem, EndProgram
from source.utils import get_last_config, update_last_config


def capture_exception(reader, dsn):
    client = Client(dsn, transport=HTTPTransport, raise_send_errors=True)

    if "--mock" in sys.argv or "-m" in sys.argv:
        print(traceback.format_exc())

    extra = {
        "Platform": platform.platform(),
    }
    if reader is not None:
        extra["reader"] = reader.get_config()

    try:
        client.captureException(
            extra=extra,
            tags={
                "system": platform.system()
            }
        )
    except (OSError, URLError, HTTPError):
        print("Something went wrong, computer not connected to the internet, error report not sent. Error saved to a file.")

        directory = Path(Path(__file__).parent, "exceptions")
        if not directory.exists():
            directory.mkdir()

        with open(Path(directory, "{}.log".format(datetime.datetime.now().isoformat())), "w") as fl:
            fl.write(traceback.format_exc())
            fl.write("\n\n")
            pprint.pprint(extra or {}, fl)

    else:
        print("Something went terribly wrong, error report sent.")


if __name__ == "__main__":
    port = None
    reader = None
    try:
        last_port = get_last_config("port")

        try:
            port = input("Enter the com port ({} if empty): ".format(last_port))
        except (EOFError, KeyboardInterrupt):
            print("")
            exit()

        if not port:
            port = last_port
        try:
            if port != last_port:
                update_last_config("port", port)

            reader = Interface(port, mock="--mock" in sys.argv or "-m" in sys.argv)
        except ConnectionProblem:
            print("Couldn't connect to the device")
        else:
            try:
                reader.main()
            except EndProgram:
                pass
    except SystemExit:
        pass
    except:
        dsn = get_last_config("sentry_dsn")
        if dsn:
            capture_exception(reader, dsn)
        else:
            print("Something went terribly wrong, but no sentry dsn is setup, so we can't let the author know.")
